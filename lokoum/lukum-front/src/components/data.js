export const sliderItems = [
    {
      id: 1,
      img: "20220628_132753 (1)",
      title: "Delicious Honey",
      desc: "ORGANIC HONEY FOR HEALTHY LIFE.",
      bg: "f5fafd",
    },
    {
      id: 2,
      img: "20220628_132042",
      title: "100% Pure",
      desc: "FROM OUR HIVE TO YOUR KITCHEN.",
      bg: "fcf1ed",
    },
    {
      id: 3,
      img: "20220628_140344 (1)",
      title: "Your Daily Doze",
      desc: "NATURAL FOOD FOR YOUR BODY.",
      bg: "fbf0f4",
    },
  ];