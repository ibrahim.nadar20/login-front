import React from "react";
import "./about.css";
function AboutSection() {
  return (
    <div>
      <section className="about-section">
        <div className="container">
          <div className="row">
            <div className="content-column col-lg-6 col-md-12 col-sm-12 order-2">
              <div className="inner-column">
                <div className="sec-title">
                  <span className="title">من نحن </span>
                  <h2>جمعية الكشاف المسلم في لبنان</h2>
                </div>
                <div className="text">
                  تكمن أهمية صفحة “من نحن”، والتي هي الجزء الأهمّ من الملف
                  التعريفي لأي شركة، في كونها بمثابة السيرة الذاتية للمنشأة،
                  إنها تقدم شركتك للآخرين، لعملائك ولعملائك المحتملين، وللزوار
                  الفضوليين الذين قادهم العثور على رابط موقع الشركة في مكان ما،
                  أو قادتهم محركات البحث كي يطرقوا باب الموقع رغبة في الاطلاع
                  على: ما الذي تقدمه، من أنت، متى بدأت، ما الذي تفعله، وكيف تفعل
                  ذلك باحترافية؟
                </div>
                <div className="text">
                  تكمن أهمية صفحة “من نحن”، والتي هي الجزء الأهمّ من الملف
                  التعريفي لأي شركة، في كونها بمثابة السيرة الذاتية للمنشأة،
                  إنها تقدم شركتك للآخرين، لعملائك ولعملائك المحتملين، وللزوار
                  الفضوليين الذين قادهم العثور على رابط موقع الشركة في مكان ما،
                  أو قادتهم محركات البحث كي يطرقوا باب الموقع رغبة في الاطلاع
                  على: ما الذي تقدمه، من أنت، متى بدأت، ما الذي تفعله، وكيف تفعل
                  ذلك باحترافية؟
                </div>
                <div className="btn-box">
                  <a href="#" className="theme-btn btn-style-one">
                    تواصل معنا
                  </a>
                </div>
              </div>
            </div>

            <div className="image-column col-lg-6 col-md-12 col-sm-12">
              <div className="inner-column wow fadeInLeft">
                <div className="author-desc">
                  <h2>مفوضية الجنوب </h2>
                  <span>Muslim Scout</span>
                </div>
                <figure className="image-1">
                  <a href="#" className="lightbox-image" data-fancybox="images">
                    <img
                      title="Rahul Kumar Yadav"
                      src="https://i.ibb.co/QP6Nmpf/image-1-about.jpg"
                      alt=""
                    />
                  </a>
                </figure>
              </div>
            </div>
          </div>
          {/* <div className="sec-title">
            <span className="title">أهدافنا المستقبلية</span>
            <h2>مجتمع تسوده القيم والاخلاق والروح الكشفية</h2>
          </div>
          <div className="text">
            عندما ينقر الزائر على رابط صفحة “من نحن” فهو يمنحك فرصة الحديث معه
            لبضع ثوانٍ، لذلك ينبغي أن يكون حديثك مثيرا للاهتمام، وملفتا، تخيل
            أنك في اجتماع مع أحد العملاء لتقنعه بشراء المنتجات 
          </div>
          <div className="text">
          عندما ينقر الزائر على رابط صفحة “من نحن” فهو يمنحك فرصة الحديث معه
            لبضع ثوانٍ، لذلك ينبغي أن يكون حديثك مثيرا للاهتمام، وملفتا، تخيل
            أنك في اجتماع مع أحد العملاء لتقنعه بشراء المنتجات 
          </div>
          <div className="text">
          عندما ينقر الزائر على رابط صفحة “من نحن” فهو يمنحك فرصة الحديث معه
            لبضع ثوانٍ، لذلك ينبغي أن يكون حديثك مثيرا للاهتمام، وملفتا، تخيل
            أنك في اجتماع مع أحد العملاء لتقنعه بشراء المنتجات 
          </div>
          <div className="text">
          عندما ينقر الزائر على رابط صفحة “من نحن” فهو يمنحك فرصة الحديث معه
            لبضع ثوانٍ، لذلك ينبغي أن يكون حديثك مثيرا للاهتمام، وملفتا، تخيل
            أنك في اجتماع مع أحد العملاء لتقنعه بشراء المنتجات 
          </div>
          <div className="text">
          عندما ينقر الزائر على رابط صفحة “من نحن” فهو يمنحك فرصة الحديث معه
            لبضع ثوانٍ، لذلك ينبغي أن يكون حديثك مثيرا للاهتمام، وملفتا، تخيل
            أنك في اجتماع مع أحد العملاء لتقنعه بشراء المنتجات 
          </div> */}
        </div>
      </section>
    </div>
  );
}

export default AboutSection;
