import React from "react";
import "./icons.css";
import donate from "../../../Assets/donate.png";
import hand from "../../../Assets/hand.png";
import flower from "../../../Assets/flower.png";
import nature from "../../../Assets/nature.png";
import fire from "../../../Assets/fire.png";

function Icons() {
  return (
    <div className="parent">
      <img src={nature} alt="" />
      <img src={fire} alt="" />
      <img src={flower} alt="" /> 
      <img src={hand} alt="" />
      <img src={donate} alt="" />
     
    </div>
  );
}

export default Icons;
