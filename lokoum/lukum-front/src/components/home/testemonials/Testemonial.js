 import React, { useEffect, useState } from 'react';
import TestiMonialsDetails from './TestiMonialsDetails';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
// import userPic from '../../image/user-one.png';
import './testemonial.css'

const Testemonial = () => {
  
    const testiMonials = [
        {
            name: 'المهندس قائد رامي بشاشة',
            description: 'تكمن أهمية صفحة “من نحن”، والتي هي الجزء الأهمّ من الملف التعريفي لأي شركة، في كونها بمثابة السيرة الذاتية للمنشأة، إنها تقدم شركتك للآخرين، لعملائك ولعملائك المحتملين، وللزوار الفضوليين الذين قادهم',
            address: "مفوض الجنوب",
            img: 'https://i.ibb.co/hgGJc8d/Gareth-Bale.jpg'
        },
        {
            name: 'Brandon Savage',
            description: 'تكمن أهمية صفحة “من نحن”، والتي هي الجزء الأهمّ من الملف التعريفي لأي شركة، في كونها بمثابة السيرة الذاتية للمنشأة، إنها تقدم شركتك للآخرين، لعملائك ولعملائك المحتملين، وللزوار الفضوليين الذين قادهم',
            address: 'USA',
            img: 'https://i.ibb.co/z7Kp6yr/np-file-33188.jpg'
        },
        {
            name: 'Steve Burns',
            description: 'تكمن أهمية صفحة “من نحن”، والتي هي الجزء الأهمّ من الملف التعريفي لأي شركة، في كونها بمثابة السيرة الذاتية للمنشأة، إنها تقدم شركتك للآخرين، لعملائك ولعملائك المحتملين، وللزوار الفضوليين الذين قادهم',
            address: 'USA',
            img: 'https://i.ibb.co/CP5sj7g/2856040-58866808-2560-1440.jpg'
        },
        {
            name: 'Kevin Canlas',
            description: 'تكمن أهمية صفحة “من نحن”، والتي هي الجزء الأهمّ من الملف التعريفي لأي شركة، في كونها بمثابة السيرة الذاتية للمنشأة، إنها تقدم شركتك للآخرين، لعملائك ولعملائك المحتملين، وللزوار الفضوليين الذين قادهم.',
            address: 'USA',
            img: 'https://i.ibb.co/10SYccm/1552313010-354215-noticia-normal.jpg'
        },
    ]
    //Owl Carousel Settings
    const options = {
        loop: true,
        center: true,
        items: 3,
        margin: 0,
        autoplay: true,
        dots: true,
        autoplayTimeout: 8500,
        smartSpeed: 450,
        nav: false,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 2
            },
            1170:{
              items:3
              
            }
            ,
            1180:{
              items:2
              
            },
            1190:{
              items:3
            }

             
        }
    };
    return (
        <section id="testimonial" className="testimonials pt-70 pb-70">
            <div className="container mt-5">
                <h4 className="miniTitle text-center">التوصيات</h4>
                <div className="text-center ">
                    <h3 className="sectionTitle">ماذا قالوا عنّا؟</h3>
                </div>
              
                <div className="row">
                    <div className="col-md-12">
                        <OwlCarousel id="customer-testimonoals" className="owl-carousel owl-theme" {...options}>
                            {
                                testiMonials.length === 0 ?
                                    <div className="item">
                                        <div className="shadow-effect">
                                            {/* <img class="img-circle" src={userPic} />  */}

                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                                        </div>
                                        <div className="testimonial-name">
                                            <h5>Rajon Rony</h5>
                                            <small>ITALY</small>
                                        </div>
                                    </div> :
                                    testiMonials.map(testiMonialDetail => {
                                        return (
                                            <TestiMonialsDetails testiMonialDetail={testiMonialDetail} key={testiMonialDetail._key} />

                                        )
                                    })
                            }
                        </OwlCarousel>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default Testemonial;
 