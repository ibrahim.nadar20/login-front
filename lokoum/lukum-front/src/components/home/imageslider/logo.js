import ImageSlider from "./LogoSlider";
const App = () => {
  const slides = [
    { url: "https://cdn.pixabay.com/photo/2018/04/18/14/26/background-image-3330583__480.jpg", title: "beach" },
    { url: "https://i.ibb.co/QP6Nmpf/image-1-about.jpg", title: "boat" },
    { url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR0KdwnEMGpNehIRH6-oOWR-4s-kZqZy901ZA&usqp=CAU", title: "forest" },
    { url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSkTn3d6bgK3lKPrsP-x-F4pjfQzCqcgV8aMw&usqp=CAU", title: "city" },
    { url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRdJ0rJteMDPjRBKG57NX5LhH5I54fD9bO8fw&usqp=CAU", title: "italy" },
  ];
  const containerStyles = {
    width: "500px",
    height: "280px",
    margin: "0 auto",
  };
  return (
    <div>
 
      <div style={containerStyles}>
        <ImageSlider slides={slides} />
      </div>
    </div>
  );
};

export default App;