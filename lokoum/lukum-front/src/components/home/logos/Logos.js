import React from "react";
import "./logos.css";
function Logos() {
  return (
    
 
        <div className="logos">
        <div className="logos-detail">
            <img
            className="logos-img"
            src="https://99designs-blog.imgix.net/blog/wp-content/uploads/2019/08/1920x1080-brands-amazon-logo.jpg?auto=format&q=60&fit=max&w=930"
            alt="logo"
            />
            <img
            className="logos-img"
            src="https://99designs-blog.imgix.net/blog/wp-content/uploads/2019/08/1920x1080-brands-amazon-logo.jpg?auto=format&q=60&fit=max&w=930"
            alt="logo"
            />
            <img
            className="logos-img"
            src="https://99designs-blog.imgix.net/blog/wp-content/uploads/2019/08/1920x1080-brands-amazon-logo.jpg?auto=format&q=60&fit=max&w=930"
            alt="logo"
            />
            <img
            className="logos-img"
            src="https://99designs-blog.imgix.net/blog/wp-content/uploads/2019/08/1920x1080-brands-amazon-logo.jpg?auto=format&q=60&fit=max&w=930"
            alt="logo"
            />
            <img
            className="logos-img"
            src="https://99designs-blog.imgix.net/blog/wp-content/uploads/2019/08/1920x1080-brands-amazon-logo.jpg?auto=format&q=60&fit=max&w=930"
            alt="logo"
            />
        </div>
        <div className="logos-detail">
            <img
            className="logos-img"
            src="https://99designs-blog.imgix.net/blog/wp-content/uploads/2019/08/1920x1080-brands-amazon-logo.jpg?auto=format&q=60&fit=max&w=930"
            alt="logo"
            />
            <img
            className="logos-img"
            src="https://99designs-blog.imgix.net/blog/wp-content/uploads/2019/08/1920x1080-brands-amazon-logo.jpg?auto=format&q=60&fit=max&w=930"
            alt="logo"
            />
            <img
            className="logos-img"
            src="https://99designs-blog.imgix.net/blog/wp-content/uploads/2019/08/1920x1080-brands-amazon-logo.jpg?auto=format&q=60&fit=max&w=930"
            alt="logo"
            />
            <img
            className="logos-img"
            src="https://99designs-blog.imgix.net/blog/wp-content/uploads/2019/08/1920x1080-brands-amazon-logo.jpg?auto=format&q=60&fit=max&w=930"
            alt="logo"
            />
            <img
            className="logos-img"
            src="https://99designs-blog.imgix.net/blog/wp-content/uploads/2019/08/1920x1080-brands-amazon-logo.jpg?auto=format&q=60&fit=max&w=930"
            alt="logo"
            />
        </div>
        </div>
  );
}

export default Logos;




// import React from 'react';
// import PropTypes from 'prop-types';
// import { withStyles } from '@material-ui/core/styles';
// import GridList from '@material-ui/core/GridList';
// import GridListTile from '@material-ui/core/GridListTile';
// import GridListTileBar from '@material-ui/core/GridListTileBar';
// import IconButton from '@material-ui/core/IconButton';
// import StarBorderIcon from '@material-ui/icons/StarBorder';
// import tileData from './tileData';

// const styles = theme => ({
//   root: {
//     display: 'flex',
//     flexWrap: 'wrap',
//     justifyContent: 'space-around',
//     overflow: 'hidden',
//     backgroundColor: theme.palette.background.paper,
//   },
//   gridList: {
//     flexWrap: 'nowrap',
//     // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
//     transform: 'translateZ(0)',
//   },
//   title: {
//     color: theme.palette.primary.light,
//   },
//   titleBar: {
//     background:
//       'linear-gradient(to top, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)',
//   },
// });

// /**
//  * The example data is structured as follows:
//  *
//  * import image from 'path/to/image.jpg';
//  * [etc...]
//  *
//  * const tileData = [
//  *   {
//  *     img: image,
//  *     title: 'Image',
//  *     author: 'author',
//  *   },
//  *   {
//  *     [etc...]
//  *   },
//  * ];
//  */
// function SingleLineGridList(props) {
//   const { classes } = props;

//   return (
//     <div className={classes.root}>
//       <GridList className={classes.gridList} cols={2.5}>
//         {tileData.map(tile => (
//           <GridListTile key={tile.img}>
//             <img src={tile.img} alt={tile.title} />
//             <GridListTileBar
//               title={tile.title}
//               classes={{
//                 root: classes.titleBar,
//                 title: classes.title,
//               }}
//               actionIcon={
//                 <IconButton>
//                   <StarBorderIcon className={classes.title} />
//                 </IconButton>
//               }
//             />
//           </GridListTile>
//         ))}
//       </GridList>
//     </div>
//   );
// }

// SingleLineGridList.propTypes = {
//   classes: PropTypes.object.isRequired,
// };

// export default withStyles(styles)(SingleLineGridList);

