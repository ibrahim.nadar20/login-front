import React from "react";
import "./App.css"; 
import Navbar from "./components/Navbar/Navbar";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Home from "./pages/navbar/Home";
import About from "./pages/navbar/About";
import Contact from '../src/pages/navbar/Contact';
import Blog from "./pages/navbar/Blog";
// import Cover from "./components/Navbar/Cover";
// import HeroSlider from "./components/slider/Slider";

function App() {
  return (
    <Router>
      {/* <Navbar /> */}
      <Routes>
      <Route
              path="/"
              element={
                <>
                {/* <Cover /> */}
                  {/* <Navbar />   */}
                  <Home />
                 
                
                
                </>
              }
            />
             <Route
              path="/about"
              element={
                <>
                  <Navbar />
                  <About />
                
                
                </>
              }
            />
             <Route
              path="/contact"
              element={
                <>
                  <Navbar />
                  <Contact />
                
                
                </>
              }
            />
            <Route
              path="/blog"
              element={
                <>
                  <Navbar />
              <Blog />
                
                
                </>
              }
            />
            {/* <Route path="/navbar" element={<Navbar />} /> */}
        {/* <Route  path="/" element={<Home />} />
        <Route exact path="/about" component={About} />
        <Route exact path="/contact" component={Contact} />  */}
      </Routes>
    </Router>
  );
}

export default App;
