import React from "react";
import NavComponent from "../../components/slider/NavComponent";
import HeroSlider from "../../components/slider/Slider";
// import Logo from "../../components/home/imageslider/logo"
import AboutSection from "../../components/home/about/AboutSection";
//  import Logos from "../../components/home/logos/Logos";
 import Icons from "../../components/home/icons/Icons";
import Testemonial from "../../components/home/testemonials/Testemonial";
 
function Home() {
  return (
    <>
      <NavComponent />  
      <HeroSlider />
      <Icons />
      <AboutSection />
      <Testemonial />
      {/* <Logos /> */}
     {/* <Logo /> */}
  
   
    </>
  );
}

export default Home;
